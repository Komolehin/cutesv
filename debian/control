Source: cutesv
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Steffen Moeller <moeller@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3,
               python3-setuptools
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/cutesv
Vcs-Git: https://salsa.debian.org/med-team/cutesv.git
Homepage: https://github.com/tjiangHIT/cuteSV
Rules-Requires-Root: no

Package: cutesv
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-cigar,
         python3-vcf
Description: comprehensive discovery of structural variations of genomic sequences
 Long-read sequencing enables the comprehensive discovery of structural
 variations (SVs). However, it is still non-trivial to achieve high
 sensitivity and performance simultaneously due to the complex SV
 characteristics implied by noisy long reads.
 .
 cuteSV is a sensitive, fast and scalable long-read-based SV detection
 approach. cuteSV uses tailored methods to collect the signatures of
 various types of SVs and employs a clustering-and-refinement method to
 analyze the signatures to implement sensitive SV detection. Benchmarks
 on real Pacific Biosciences (PacBio) and Oxford Nanopore Technology
 (ONT) datasets demonstrate that cuteSV has better yields and scalability
 than state-of-the-art tools.
